package org.socraticgrid.fhir.generated;

import org.socraticgrid.fhir.generated.IPatient;
import ca.uhn.fhir.model.dstu2.resource.Patient;
import ca.uhn.fhir.model.primitive.IdDt;
import ca.uhn.fhir.model.primitive.CodeDt;
import ca.uhn.fhir.model.dstu2.composite.NarrativeDt;
import ca.uhn.fhir.model.dstu2.composite.ContainedDt;
import ca.uhn.fhir.model.dstu2.composite.IdentifierDt;
import java.util.List;
import ca.uhn.fhir.model.dstu2.composite.HumanNameDt;
import ca.uhn.fhir.model.dstu2.composite.ContactPointDt;
import ca.uhn.fhir.model.dstu2.valueset.AdministrativeGenderEnum;
import ca.uhn.fhir.model.primitive.BoundCodeDt;
import ca.uhn.fhir.model.primitive.DateDt;
import java.util.Date;
import ca.uhn.fhir.model.primitive.BooleanDt;
import ca.uhn.fhir.model.primitive.DateTimeDt;
import ca.uhn.fhir.model.dstu2.composite.AddressDt;
import ca.uhn.fhir.model.dstu2.valueset.MaritalStatusCodesEnum;
import ca.uhn.fhir.model.dstu2.composite.BoundCodeableConceptDt;
import ca.uhn.fhir.model.primitive.IntegerDt;
import ca.uhn.fhir.model.dstu2.composite.AttachmentDt;
import ca.uhn.fhir.model.dstu2.resource.Organization;

public class PatientAdapter implements IPatient
{

   private Patient adaptedClass = new Patient();

   public IdDt getId()
   {
      return adaptedClass.getId();
   }

   public void setId(IdDt param)
   {
      adaptedClass.setId(param);
   }

   public CodeDt getLanguage()
   {
      return adaptedClass.getLanguage();
   }

   public void setLanguage(CodeDt param)
   {
      adaptedClass.setLanguage(param);
   }

   public NarrativeDt getText()
   {
      return adaptedClass.getText();
   }

   public void setText(NarrativeDt param)
   {
      adaptedClass.setText(param);
   }

   public ContainedDt getContained()
   {
      return adaptedClass.getContained();
   }

   public void setContained(ContainedDt param)
   {
      adaptedClass.setContained(param);
   }

   public List<IdentifierDt> getIdentifier()
   {
      return adaptedClass.getIdentifier();
   }

   public void setIdentifier(List<IdentifierDt> param)
   {
      adaptedClass.setIdentifier(param);
   }

   public void addIdentifier(IdentifierDt param)
   {
      adaptedClass.getIdentifier().add(param);
   }

   public List<HumanNameDt> getName()
   {
      return adaptedClass.getName();
   }

   public void setName(List<HumanNameDt> param)
   {
      adaptedClass.setName(param);
   }

   public void addName(HumanNameDt param)
   {
      adaptedClass.getName().add(param);
   }

   public List<ContactPointDt> getTelecom()
   {
      return adaptedClass.getTelecom();
   }

   public void setTelecom(List<ContactPointDt> param)
   {
      adaptedClass.setTelecom(param);
   }

   public void addTelecom(ContactPointDt param)
   {
      adaptedClass.getTelecom().add(param);
   }

   public BoundCodeDt<AdministrativeGenderEnum> getGenderElement()
   {
      return adaptedClass.getGenderElement();
   }

   public String getGender()
   {
      return adaptedClass.getGender();
   }

   public void setGender(String param)
   {
      adaptedClass
            .setGender(ca.uhn.fhir.model.dstu2.valueset.AdministrativeGenderEnum
                  .valueOf(param));
   }

   public void setGender(BoundCodeDt<AdministrativeGenderEnum> param)
   {
      adaptedClass.setGender(param);
   }

   public DateDt getBirthDateElement()
   {
      return adaptedClass.getBirthDateElement();
   }

   public Date getBirthDate()
   {
      return adaptedClass.getBirthDate();
   }

   public void setBirthDate(Date param)
   {
      adaptedClass
            .setBirthDate(new ca.uhn.fhir.model.primitive.DateDt(param));
   }

   public void setBirthDate(DateDt param)
   {
      adaptedClass.setBirthDate(param);
   }

   public BooleanDt getDeceasedBooleanElement()
   {
      if (adaptedClass.getDeceased() != null
            && adaptedClass.getDeceased() instanceof ca.uhn.fhir.model.primitive.BooleanDt)
      {
         return (ca.uhn.fhir.model.primitive.BooleanDt) adaptedClass
               .getDeceased();
      }
      else
      {
         return null;
      }
   }

   public Boolean getDeceasedBoolean()
   {
      if (adaptedClass.getDeceased() != null
            && adaptedClass.getDeceased() instanceof ca.uhn.fhir.model.primitive.BooleanDt)
      {
         return ((ca.uhn.fhir.model.primitive.BooleanDt) adaptedClass
               .getDeceased()).getValue();
      }
      else
      {
         return null;
      }
   }

   public void setDeceasedBoolean(BooleanDt param)
   {
      adaptedClass.setDeceased(param);
   }

   public void setDeceasedBoolean(Boolean param)
   {
      adaptedClass.setDeceased(new ca.uhn.fhir.model.primitive.BooleanDt(
            param));
   }

   public DateTimeDt getDeceasedDateTimeElement()
   {
      if (adaptedClass.getDeceased() != null
            && adaptedClass.getDeceased() instanceof ca.uhn.fhir.model.primitive.DateTimeDt)
      {
         return (ca.uhn.fhir.model.primitive.DateTimeDt) adaptedClass
               .getDeceased();
      }
      else
      {
         return null;
      }
   }

   public Date getDeceasedDateTime()
   {
      if (adaptedClass.getDeceased() != null
            && adaptedClass.getDeceased() instanceof ca.uhn.fhir.model.primitive.DateTimeDt)
      {
         return ((ca.uhn.fhir.model.primitive.DateTimeDt) adaptedClass
               .getDeceased()).getValue();
      }
      else
      {
         return null;
      }
   }

   public void setDeceasedDateTime(DateTimeDt param)
   {
      adaptedClass.setDeceased(param);
   }

   public void setDeceasedDateTime(Date param)
   {
      adaptedClass.setDeceased(new ca.uhn.fhir.model.primitive.DateTimeDt(
            param));
   }

   public List<AddressDt> getAddress()
   {
      return adaptedClass.getAddress();
   }

   public void setAddress(List<AddressDt> param)
   {
      adaptedClass.setAddress(param);
   }

   public void addAddress(AddressDt param)
   {
      adaptedClass.getAddress().add(param);
   }

   public BoundCodeableConceptDt<MaritalStatusCodesEnum> getMaritalStatus()
   {
      return adaptedClass.getMaritalStatus();
   }

   public void setMaritalStatus(
         BoundCodeableConceptDt<MaritalStatusCodesEnum> param)
   {
      adaptedClass.setMaritalStatus(param);
   }

   public BooleanDt getMultipleBirthBooleanElement()
   {
      if (adaptedClass.getMultipleBirth() != null
            && adaptedClass.getMultipleBirth() instanceof ca.uhn.fhir.model.primitive.BooleanDt)
      {
         return (ca.uhn.fhir.model.primitive.BooleanDt) adaptedClass
               .getMultipleBirth();
      }
      else
      {
         return null;
      }
   }

   public Boolean getMultipleBirthBoolean()
   {
      if (adaptedClass.getMultipleBirth() != null
            && adaptedClass.getMultipleBirth() instanceof ca.uhn.fhir.model.primitive.BooleanDt)
      {
         return ((ca.uhn.fhir.model.primitive.BooleanDt) adaptedClass
               .getMultipleBirth()).getValue();
      }
      else
      {
         return null;
      }
   }

   public void setMultipleBirthBoolean(BooleanDt param)
   {
      adaptedClass.setMultipleBirth(param);
   }

   public void setMultipleBirthBoolean(Boolean param)
   {
      adaptedClass
            .setMultipleBirth(new ca.uhn.fhir.model.primitive.BooleanDt(
                  param));
   }

   public IntegerDt getMultipleBirthIntegerElement()
   {
      if (adaptedClass.getMultipleBirth() != null
            && adaptedClass.getMultipleBirth() instanceof ca.uhn.fhir.model.primitive.IntegerDt)
      {
         return (ca.uhn.fhir.model.primitive.IntegerDt) adaptedClass
               .getMultipleBirth();
      }
      else
      {
         return null;
      }
   }

   public Integer getMultipleBirthInteger()
   {
      if (adaptedClass.getMultipleBirth() != null
            && adaptedClass.getMultipleBirth() instanceof ca.uhn.fhir.model.primitive.IntegerDt)
      {
         return ((ca.uhn.fhir.model.primitive.IntegerDt) adaptedClass
               .getMultipleBirth()).getValue();
      }
      else
      {
         return null;
      }
   }

   public void setMultipleBirthInteger(IntegerDt param)
   {
      adaptedClass.setMultipleBirth(param);
   }

   public void setMultipleBirthInteger(Integer param)
   {
      adaptedClass
            .setMultipleBirth(new ca.uhn.fhir.model.primitive.IntegerDt(
                  param));
   }

   public List<AttachmentDt> getPhoto()
   {
      return adaptedClass.getPhoto();
   }

   public void setPhoto(List<AttachmentDt> param)
   {
      adaptedClass.setPhoto(param);
   }

   public void addPhoto(AttachmentDt param)
   {
      adaptedClass.getPhoto().add(param);
   }

   public List<Patient.Contact> getContact()
   {
      return adaptedClass.getContact();
   }

   public void setContact(List<Patient.Contact> param)
   {
      adaptedClass.setContact(param);
   }

   public void addContact(Patient.Contact param)
   {
      adaptedClass.getContact().add(param);
   }

   public Patient.Animal getAnimal()
   {
      return adaptedClass.getAnimal();
   }

   public void setAnimal(Patient.Animal param)
   {
      adaptedClass.setAnimal(param);
   }

   public List<Patient.Communication> getCommunication()
   {
      return adaptedClass.getCommunication();
   }

   public void setCommunication(List<Patient.Communication> param)
   {
      adaptedClass.setCommunication(param);
   }

   public void addCommunication(Patient.Communication param)
   {
      adaptedClass.getCommunication().add(param);
   }

   public Organization getManagingOrganizationResource()
   {
      if (adaptedClass.getManagingOrganization().getResource() instanceof ca.uhn.fhir.model.dstu2.resource.Organization)
      {
         return (ca.uhn.fhir.model.dstu2.resource.Organization) adaptedClass
               .getManagingOrganization().getResource();
      }
      else
      {
         return null;
      }
   }

   public void setManagingOrganizationResource(Organization param)
   {
      adaptedClass.getManagingOrganization().setResource(param);
   }

   public List<Patient.Link> getLink()
   {
      return adaptedClass.getLink();
   }

   public void setLink(List<Patient.Link> param)
   {
      adaptedClass.setLink(param);
   }

   public void addLink(Patient.Link param)
   {
      adaptedClass.getLink().add(param);
   }

   public BooleanDt getActiveElement()
   {
      return adaptedClass.getActiveElement();
   }

   public Boolean getActive()
   {
      return adaptedClass.getActive();
   }

   public void setActive(Boolean param)
   {
      adaptedClass
            .setActive(new ca.uhn.fhir.model.primitive.BooleanDt(param));
   }

   public void setActive(BooleanDt param)
   {
      adaptedClass.setActive(param);
   }

   public Patient getAdaptee()
   {
      return adaptedClass;
   }

   public void setAdaptee(Patient param)
   {
      this.adaptedClass = param;
   }
}