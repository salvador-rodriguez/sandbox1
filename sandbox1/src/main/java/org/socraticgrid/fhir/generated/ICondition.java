package org.socraticgrid.fhir.generated;

import ca.uhn.fhir.model.primitive.IdDt;
import ca.uhn.fhir.model.primitive.CodeDt;
import ca.uhn.fhir.model.dstu2.composite.NarrativeDt;
import ca.uhn.fhir.model.dstu2.composite.ContainedDt;
import ca.uhn.fhir.model.dstu2.composite.IdentifierDt;
import java.util.List;
import ca.uhn.fhir.model.dstu2.resource.Patient;
import ca.uhn.fhir.model.dstu2.resource.Encounter;
import ca.uhn.fhir.model.dstu2.resource.Practitioner;
import ca.uhn.fhir.model.primitive.DateDt;
import java.util.Date;
import ca.uhn.fhir.model.dstu2.composite.CodingDt;
import java.util.ArrayList;
import java.util.Iterator;
import ca.uhn.fhir.model.dstu2.composite.CodeableConceptDt;
import ca.uhn.fhir.model.dstu2.valueset.ConditionClinicalStatusEnum;
import ca.uhn.fhir.model.primitive.BoundCodeDt;
import ca.uhn.fhir.model.primitive.DateTimeDt;
import ca.uhn.fhir.model.dstu2.composite.AgeDt;
import ca.uhn.fhir.model.dstu2.composite.TimingDt;
import ca.uhn.fhir.model.dstu2.composite.RangeDt;
import ca.uhn.fhir.model.primitive.StringDt;
import ca.uhn.fhir.model.primitive.BooleanDt;
import ca.uhn.fhir.model.dstu2.resource.Condition;

public interface ICondition
{

   public IdDt getId();

   public void setId(IdDt param);

   public CodeDt getLanguage();

   public void setLanguage(CodeDt param);

   public NarrativeDt getText();

   public void setText(NarrativeDt param);

   public ContainedDt getContained();

   public void setContained(ContainedDt param);

   public List<IdentifierDt> getIdentifier();

   public void setIdentifier(List<IdentifierDt> param);

   public void addIdentifier(IdentifierDt param);

   public Patient getPatientResource();

   public void setPatientResource(Patient param);

   public Encounter getEncounterResource();

   public void setEncounterResource(Encounter param);

   public Practitioner getAsserterPractitionerResource();

   public void setAsserterResource(Practitioner param);

   public Patient getAsserterPatientResource();

   public void setAsserterResource(Patient param);

   public DateDt getDateAssertedElement();

   public Date getDateAsserted();

   public void setDateAsserted(Date param);

   public void setDateAsserted(DateDt param);

   public CodeableConceptDt getCode();

   public void setCode(CodeableConceptDt param);

   public List<String> getCodeAsStringList();

   public CodeableConceptDt getCategory();

   public void setCategory(CodeableConceptDt param);

   public List<String> getCategoryAsStringList();

   public BoundCodeDt<ConditionClinicalStatusEnum> getClinicalStatusElement();

   public String getClinicalStatus();

   public void setClinicalStatus(String param);

   public void setClinicalStatus(BoundCodeDt<ConditionClinicalStatusEnum> param);

   public CodeableConceptDt getSeverity();

   public void setSeverity(CodeableConceptDt param);

   public List<String> getSeverityAsStringList();

   public DateTimeDt getOnsetDateTimeElement();

   public Date getOnsetDateTime();

   public void setOnsetDateTime(DateTimeDt param);

   public void setOnsetDateTime(Date param);

   public AgeDt getOnsetAge();

   public void setOnsetAge(AgeDt param);

   public TimingDt getOnsetPeriod();

   public void setOnsetPeriod(TimingDt param);

   public RangeDt getOnsetRange();

   public void setOnsetRange(RangeDt param);

   public StringDt getOnsetStringElement();

   public String getOnsetString();

   public void setOnsetString(StringDt param);

   public void setOnsetString(String param);

   public DateDt getAbatementDateElement();

   public Date getAbatementDate();

   public void setAbatementDate(DateDt param);

   public void setAbatementDate(Date param);

   public AgeDt getAbatementAge();

   public void setAbatementAge(AgeDt param);

   public BooleanDt getAbatementBooleanElement();

   public Boolean getAbatementBoolean();

   public void setAbatementBoolean(BooleanDt param);

   public void setAbatementBoolean(Boolean param);

   public TimingDt getAbatementPeriod();

   public void setAbatementPeriod(TimingDt param);

   public RangeDt getAbatementRange();

   public void setAbatementRange(RangeDt param);

   public StringDt getAbatementStringElement();

   public String getAbatementString();

   public void setAbatementString(StringDt param);

   public void setAbatementString(String param);

   public Condition.Stage getStage();

   public void setStage(Condition.Stage param);

   public List<Condition.Evidence> getEvidence();

   public void setEvidence(List<Condition.Evidence> param);

   public void addEvidence(Condition.Evidence param);

   public List<Condition.Location> getLocation();

   public void setLocation(List<Condition.Location> param);

   public void addLocation(Condition.Location param);

   public List<Condition.DueTo> getDueTo();

   public void setDueTo(List<Condition.DueTo> param);

   public void addDueTo(Condition.DueTo param);

   public List<Condition.OccurredFollowing> getOccurredFollowing();

   public void setOccurredFollowing(List<Condition.OccurredFollowing> param);

   public void addOccurredFollowing(Condition.OccurredFollowing param);

   public StringDt getNotesElement();

   public String getNotes();

   public void setNotes(String param);

   public void setNotes(StringDt param);

   public Condition getAdaptee();

   public void setAdaptee(Condition param);
}