package org.socraticgrid.fhir.generated;

import ca.uhn.fhir.model.primitive.IdDt;
import ca.uhn.fhir.model.primitive.CodeDt;
import ca.uhn.fhir.model.dstu2.composite.NarrativeDt;
import ca.uhn.fhir.model.dstu2.composite.ContainedDt;
import ca.uhn.fhir.model.dstu2.composite.IdentifierDt;
import java.util.List;
import ca.uhn.fhir.model.dstu2.resource.Patient;
import ca.uhn.fhir.model.dstu2.composite.CodingDt;
import java.util.ArrayList;
import java.util.Iterator;
import ca.uhn.fhir.model.dstu2.composite.CodeableConceptDt;
import ca.uhn.fhir.model.primitive.DateTimeDt;
import java.util.Date;
import ca.uhn.fhir.model.dstu2.composite.TimingDt;
import ca.uhn.fhir.model.dstu2.resource.Encounter;
import ca.uhn.fhir.model.dstu2.resource.Practitioner;
import ca.uhn.fhir.model.dstu2.resource.Organization;
import ca.uhn.fhir.model.dstu2.resource.RelatedPerson;
import ca.uhn.fhir.model.dstu2.valueset.ProcedureRequestStatusEnum;
import ca.uhn.fhir.model.primitive.BoundCodeDt;
import ca.uhn.fhir.model.primitive.StringDt;
import java.lang.String;
import ca.uhn.fhir.model.primitive.BooleanDt;
import ca.uhn.fhir.model.dstu2.resource.Device;
import ca.uhn.fhir.model.dstu2.valueset.ProcedureRequestPriorityEnum;
import ca.uhn.fhir.model.dstu2.resource.ProcedureRequest;

public interface IProcedureRequest
{

   public IdDt getId();

   public void setId(IdDt param);

   public CodeDt getLanguage();

   public void setLanguage(CodeDt param);

   public NarrativeDt getText();

   public void setText(NarrativeDt param);

   public ContainedDt getContained();

   public void setContained(ContainedDt param);

   public List<IdentifierDt> getIdentifier();

   public void setIdentifier(List<IdentifierDt> param);

   public void addIdentifier(IdentifierDt param);

   public Patient getSubjectResource();

   public void setSubjectResource(Patient param);

   public CodeableConceptDt getType();

   public void setType(CodeableConceptDt param);

   public List<String> getTypeAsStringList();

   public List<ProcedureRequest.BodySite> getBodySite();

   public void setBodySite(List<ProcedureRequest.BodySite> param);

   public void addBodySite(ProcedureRequest.BodySite param);

   public List<CodeableConceptDt> getIndication();

   public void setIndication(List<CodeableConceptDt> param);

   public void addIndication(CodeableConceptDt param);

   public DateTimeDt getTimingDateTimeElement();

   public Date getTimingDateTime();

   public void setTimingDateTime(DateTimeDt param);

   public void setTimingDateTime(Date param);

   public TimingDt getTimingPeriod();

   public void setTimingPeriod(TimingDt param);

   public TimingDt getTimingTiming();

   public void setTimingTiming(TimingDt param);

   public Encounter getEncounterResource();

   public void setEncounterResource(Encounter param);

   public Practitioner getPerformerPractitionerResource();

   public void setPerformerResource(Practitioner param);

   public Organization getPerformerOrganizationResource();

   public void setPerformerResource(Organization param);

   public Patient getPerformerPatientResource();

   public void setPerformerResource(Patient param);

   public RelatedPerson getPerformerRelatedPersonResource();

   public void setPerformerResource(RelatedPerson param);

   public BoundCodeDt<ProcedureRequestStatusEnum> getStatusElement();

   public String getStatus();

   public void setStatus(String param);

   public void setStatus(BoundCodeDt<ProcedureRequestStatusEnum> param);

   public BooleanDt getAsNeededBooleanElement();

   public Boolean getAsNeededBoolean();

   public void setAsNeededBoolean(BooleanDt param);

   public void setAsNeededBoolean(Boolean param);

   public CodeableConceptDt getAsNeededCodeableConcept();

   public void setAsNeededCodeableConcept(CodeableConceptDt param);

   public DateTimeDt getOrderedOnElement();

   public Date getOrderedOn();

   public void setOrderedOn(Date param);

   public void setOrderedOn(DateTimeDt param);

   public Practitioner getOrdererPractitionerResource();

   public void setOrdererResource(Practitioner param);

   public Patient getOrdererPatientResource();

   public void setOrdererResource(Patient param);

   public RelatedPerson getOrdererRelatedPersonResource();

   public void setOrdererResource(RelatedPerson param);

   public Device getOrdererDeviceResource();

   public void setOrdererResource(Device param);

   public BoundCodeDt<ProcedureRequestPriorityEnum> getPriorityElement();

   public String getPriority();

   public void setPriority(String param);

   public void setPriority(BoundCodeDt<ProcedureRequestPriorityEnum> param);

   public ProcedureRequest getAdaptee();

   public void setAdaptee(ProcedureRequest param);
}