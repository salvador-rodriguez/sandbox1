package org.socraticgrid.fhir.generated;

import org.socraticgrid.fhir.generated.ICondition;
import ca.uhn.fhir.model.dstu2.resource.Condition;
import ca.uhn.fhir.model.primitive.IdDt;
import ca.uhn.fhir.model.primitive.CodeDt;
import ca.uhn.fhir.model.dstu2.composite.NarrativeDt;
import ca.uhn.fhir.model.dstu2.composite.ContainedDt;
import ca.uhn.fhir.model.dstu2.composite.IdentifierDt;
import java.util.List;
import ca.uhn.fhir.model.dstu2.resource.Patient;
import ca.uhn.fhir.model.dstu2.resource.Encounter;
import ca.uhn.fhir.model.dstu2.resource.Practitioner;
import ca.uhn.fhir.model.primitive.DateDt;
import java.util.Date;
import ca.uhn.fhir.model.dstu2.composite.CodingDt;
import java.util.ArrayList;
import java.util.Iterator;
import ca.uhn.fhir.model.dstu2.composite.CodeableConceptDt;
import ca.uhn.fhir.model.dstu2.valueset.ConditionClinicalStatusEnum;
import ca.uhn.fhir.model.primitive.BoundCodeDt;
import ca.uhn.fhir.model.primitive.DateTimeDt;
import ca.uhn.fhir.model.dstu2.composite.AgeDt;
import ca.uhn.fhir.model.dstu2.composite.TimingDt;
import ca.uhn.fhir.model.dstu2.composite.RangeDt;
import ca.uhn.fhir.model.primitive.StringDt;
import ca.uhn.fhir.model.primitive.BooleanDt;

public class ConditionAdapter implements ICondition
{

   private Condition adaptedClass = new Condition();

   public IdDt getId()
   {
      return adaptedClass.getId();
   }

   public void setId(IdDt param)
   {
      adaptedClass.setId(param);
   }

   public CodeDt getLanguage()
   {
      return adaptedClass.getLanguage();
   }

   public void setLanguage(CodeDt param)
   {
      adaptedClass.setLanguage(param);
   }

   public NarrativeDt getText()
   {
      return adaptedClass.getText();
   }

   public void setText(NarrativeDt param)
   {
      adaptedClass.setText(param);
   }

   public ContainedDt getContained()
   {
      return adaptedClass.getContained();
   }

   public void setContained(ContainedDt param)
   {
      adaptedClass.setContained(param);
   }

   public List<IdentifierDt> getIdentifier()
   {
      return adaptedClass.getIdentifier();
   }

   public void setIdentifier(List<IdentifierDt> param)
   {
      adaptedClass.setIdentifier(param);
   }

   public void addIdentifier(IdentifierDt param)
   {
      adaptedClass.getIdentifier().add(param);
   }

   public Patient getPatientResource()
   {
      if (adaptedClass.getPatient().getResource() instanceof ca.uhn.fhir.model.dstu2.resource.Patient)
      {
         return (ca.uhn.fhir.model.dstu2.resource.Patient) adaptedClass
               .getPatient().getResource();
      }
      else
      {
         return null;
      }
   }

   public void setPatientResource(Patient param)
   {
      adaptedClass.getPatient().setResource(param);
   }

   public Encounter getEncounterResource()
   {
      if (adaptedClass.getEncounter().getResource() instanceof ca.uhn.fhir.model.dstu2.resource.Encounter)
      {
         return (ca.uhn.fhir.model.dstu2.resource.Encounter) adaptedClass
               .getEncounter().getResource();
      }
      else
      {
         return null;
      }
   }

   public void setEncounterResource(Encounter param)
   {
      adaptedClass.getEncounter().setResource(param);
   }

   public Practitioner getAsserterPractitionerResource()
   {
      if (adaptedClass.getAsserter().getResource() instanceof ca.uhn.fhir.model.dstu2.resource.Practitioner)
      {
         return (ca.uhn.fhir.model.dstu2.resource.Practitioner) adaptedClass
               .getAsserter().getResource();
      }
      else
      {
         return null;
      }
   }

   public void setAsserterResource(Practitioner param)
   {
      adaptedClass.getAsserter().setResource(param);
   }

   public Patient getAsserterPatientResource()
   {
      if (adaptedClass.getAsserter().getResource() instanceof ca.uhn.fhir.model.dstu2.resource.Patient)
      {
         return (ca.uhn.fhir.model.dstu2.resource.Patient) adaptedClass
               .getAsserter().getResource();
      }
      else
      {
         return null;
      }
   }

   public void setAsserterResource(Patient param)
   {
      adaptedClass.getAsserter().setResource(param);
   }

   public DateDt getDateAssertedElement()
   {
      return adaptedClass.getDateAssertedElement();
   }

   public Date getDateAsserted()
   {
      return adaptedClass.getDateAsserted();
   }

   public void setDateAsserted(Date param)
   {
      adaptedClass.setDateAsserted(new ca.uhn.fhir.model.primitive.DateDt(
            param));
   }

   public void setDateAsserted(DateDt param)
   {
      adaptedClass.setDateAsserted(param);
   }

   public CodeableConceptDt getCode()
   {
      return adaptedClass.getCode();
   }

   public void setCode(CodeableConceptDt param)
   {
      adaptedClass.setCode(param);
   }

   public List<String> getCodeAsStringList() {
		List<String> codes = new ArrayList<>();
		List<CodingDt> codings = adaptedClass.getCode().getCoding();
		for (Iterator<CodingDt> iterator = codings.iterator(); iterator
				.hasNext();) {
			CodingDt codingDt = (CodingDt) iterator.next();
			codes.add(codingDt.getCode());
		}
		return codes;
	}

   public CodeableConceptDt getCategory()
   {
      return adaptedClass.getCategory();
   }

   public void setCategory(CodeableConceptDt param)
   {
      adaptedClass.setCategory(param);
   }

   public List<String> getCategoryAsStringList() {
		List<String> codes = new ArrayList<>();
		List<CodingDt> codings = adaptedClass.getCategory().getCoding();
		for (Iterator<CodingDt> iterator = codings.iterator(); iterator
				.hasNext();) {
			CodingDt codingDt = (CodingDt) iterator.next();
			codes.add(codingDt.getCode());
		}
		return codes;
	}

   public BoundCodeDt<ConditionClinicalStatusEnum> getClinicalStatusElement()
   {
      return adaptedClass.getClinicalStatusElement();
   }

   public String getClinicalStatus()
   {
      return adaptedClass.getClinicalStatus();
   }

   public void setClinicalStatus(String param)
   {
      adaptedClass
            .setClinicalStatus(ca.uhn.fhir.model.dstu2.valueset.ConditionClinicalStatusEnum
                  .valueOf(param));
   }

   public void setClinicalStatus(BoundCodeDt<ConditionClinicalStatusEnum> param)
   {
      adaptedClass.setClinicalStatus(param);
   }

   public CodeableConceptDt getSeverity()
   {
      return adaptedClass.getSeverity();
   }

   public void setSeverity(CodeableConceptDt param)
   {
      adaptedClass.setSeverity(param);
   }

   public List<String> getSeverityAsStringList() {
		List<String> codes = new ArrayList<>();
		List<CodingDt> codings = adaptedClass.getSeverity().getCoding();
		for (Iterator<CodingDt> iterator = codings.iterator(); iterator
				.hasNext();) {
			CodingDt codingDt = (CodingDt) iterator.next();
			codes.add(codingDt.getCode());
		}
		return codes;
	}

   public DateTimeDt getOnsetDateTimeElement()
   {
      if (adaptedClass.getOnset() != null
            && adaptedClass.getOnset() instanceof ca.uhn.fhir.model.primitive.DateTimeDt)
      {
         return (ca.uhn.fhir.model.primitive.DateTimeDt) adaptedClass
               .getOnset();
      }
      else
      {
         return null;
      }
   }

   public Date getOnsetDateTime()
   {
      if (adaptedClass.getOnset() != null
            && adaptedClass.getOnset() instanceof ca.uhn.fhir.model.primitive.DateTimeDt)
      {
         return ((ca.uhn.fhir.model.primitive.DateTimeDt) adaptedClass
               .getOnset()).getValue();
      }
      else
      {
         return null;
      }
   }

   public void setOnsetDateTime(DateTimeDt param)
   {
      adaptedClass.setOnset(param);
   }

   public void setOnsetDateTime(Date param)
   {
      adaptedClass
            .setOnset(new ca.uhn.fhir.model.primitive.DateTimeDt(param));
   }

   public AgeDt getOnsetAge()
   {
      if (adaptedClass.getOnset() != null
            && adaptedClass.getOnset() instanceof ca.uhn.fhir.model.dstu2.composite.AgeDt)
      {
         return (ca.uhn.fhir.model.dstu2.composite.AgeDt) adaptedClass
               .getOnset();
      }
      else
      {
         return null;
      }
   }

   public void setOnsetAge(AgeDt param)
   {
      adaptedClass.setOnset(param);
   }

   public TimingDt getOnsetPeriod()
   {
      if (adaptedClass.getOnset() != null
            && adaptedClass.getOnset() instanceof ca.uhn.fhir.model.dstu2.composite.TimingDt)
      {
         return (ca.uhn.fhir.model.dstu2.composite.TimingDt) adaptedClass
               .getOnset();
      }
      else
      {
         return null;
      }
   }

   public void setOnsetPeriod(TimingDt param)
   {
      adaptedClass.setOnset(param);
   }

   public RangeDt getOnsetRange()
   {
      if (adaptedClass.getOnset() != null
            && adaptedClass.getOnset() instanceof ca.uhn.fhir.model.dstu2.composite.RangeDt)
      {
         return (ca.uhn.fhir.model.dstu2.composite.RangeDt) adaptedClass
               .getOnset();
      }
      else
      {
         return null;
      }
   }

   public void setOnsetRange(RangeDt param)
   {
      adaptedClass.setOnset(param);
   }

   public StringDt getOnsetStringElement()
   {
      if (adaptedClass.getOnset() != null
            && adaptedClass.getOnset() instanceof ca.uhn.fhir.model.primitive.StringDt)
      {
         return (ca.uhn.fhir.model.primitive.StringDt) adaptedClass
               .getOnset();
      }
      else
      {
         return null;
      }
   }

   public String getOnsetString()
   {
      if (adaptedClass.getOnset() != null
            && adaptedClass.getOnset() instanceof ca.uhn.fhir.model.primitive.StringDt)
      {
         return ((ca.uhn.fhir.model.primitive.StringDt) adaptedClass
               .getOnset()).getValue();
      }
      else
      {
         return null;
      }
   }

   public void setOnsetString(StringDt param)
   {
      adaptedClass.setOnset(param);
   }

   public void setOnsetString(String param)
   {
      adaptedClass.setOnset(new ca.uhn.fhir.model.primitive.StringDt(param));
   }

   public DateDt getAbatementDateElement()
   {
      if (adaptedClass.getAbatement() != null
            && adaptedClass.getAbatement() instanceof ca.uhn.fhir.model.primitive.DateDt)
      {
         return (ca.uhn.fhir.model.primitive.DateDt) adaptedClass
               .getAbatement();
      }
      else
      {
         return null;
      }
   }

   public Date getAbatementDate()
   {
      if (adaptedClass.getAbatement() != null
            && adaptedClass.getAbatement() instanceof ca.uhn.fhir.model.primitive.DateDt)
      {
         return ((ca.uhn.fhir.model.primitive.DateDt) adaptedClass
               .getAbatement()).getValue();
      }
      else
      {
         return null;
      }
   }

   public void setAbatementDate(DateDt param)
   {
      adaptedClass.setAbatement(param);
   }

   public void setAbatementDate(Date param)
   {
      adaptedClass
            .setAbatement(new ca.uhn.fhir.model.primitive.DateDt(param));
   }

   public AgeDt getAbatementAge()
   {
      if (adaptedClass.getAbatement() != null
            && adaptedClass.getAbatement() instanceof ca.uhn.fhir.model.dstu2.composite.AgeDt)
      {
         return (ca.uhn.fhir.model.dstu2.composite.AgeDt) adaptedClass
               .getAbatement();
      }
      else
      {
         return null;
      }
   }

   public void setAbatementAge(AgeDt param)
   {
      adaptedClass.setAbatement(param);
   }

   public BooleanDt getAbatementBooleanElement()
   {
      if (adaptedClass.getAbatement() != null
            && adaptedClass.getAbatement() instanceof ca.uhn.fhir.model.primitive.BooleanDt)
      {
         return (ca.uhn.fhir.model.primitive.BooleanDt) adaptedClass
               .getAbatement();
      }
      else
      {
         return null;
      }
   }

   public Boolean getAbatementBoolean()
   {
      if (adaptedClass.getAbatement() != null
            && adaptedClass.getAbatement() instanceof ca.uhn.fhir.model.primitive.BooleanDt)
      {
         return ((ca.uhn.fhir.model.primitive.BooleanDt) adaptedClass
               .getAbatement()).getValue();
      }
      else
      {
         return null;
      }
   }

   public void setAbatementBoolean(BooleanDt param)
   {
      adaptedClass.setAbatement(param);
   }

   public void setAbatementBoolean(Boolean param)
   {
      adaptedClass.setAbatement(new ca.uhn.fhir.model.primitive.BooleanDt(
            param));
   }

   public TimingDt getAbatementPeriod()
   {
      if (adaptedClass.getAbatement() != null
            && adaptedClass.getAbatement() instanceof ca.uhn.fhir.model.dstu2.composite.TimingDt)
      {
         return (ca.uhn.fhir.model.dstu2.composite.TimingDt) adaptedClass
               .getAbatement();
      }
      else
      {
         return null;
      }
   }

   public void setAbatementPeriod(TimingDt param)
   {
      adaptedClass.setAbatement(param);
   }

   public RangeDt getAbatementRange()
   {
      if (adaptedClass.getAbatement() != null
            && adaptedClass.getAbatement() instanceof ca.uhn.fhir.model.dstu2.composite.RangeDt)
      {
         return (ca.uhn.fhir.model.dstu2.composite.RangeDt) adaptedClass
               .getAbatement();
      }
      else
      {
         return null;
      }
   }

   public void setAbatementRange(RangeDt param)
   {
      adaptedClass.setAbatement(param);
   }

   public StringDt getAbatementStringElement()
   {
      if (adaptedClass.getAbatement() != null
            && adaptedClass.getAbatement() instanceof ca.uhn.fhir.model.primitive.StringDt)
      {
         return (ca.uhn.fhir.model.primitive.StringDt) adaptedClass
               .getAbatement();
      }
      else
      {
         return null;
      }
   }

   public String getAbatementString()
   {
      if (adaptedClass.getAbatement() != null
            && adaptedClass.getAbatement() instanceof ca.uhn.fhir.model.primitive.StringDt)
      {
         return ((ca.uhn.fhir.model.primitive.StringDt) adaptedClass
               .getAbatement()).getValue();
      }
      else
      {
         return null;
      }
   }

   public void setAbatementString(StringDt param)
   {
      adaptedClass.setAbatement(param);
   }

   public void setAbatementString(String param)
   {
      adaptedClass.setAbatement(new ca.uhn.fhir.model.primitive.StringDt(
            param));
   }

   public Condition.Stage getStage()
   {
      return adaptedClass.getStage();
   }

   public void setStage(Condition.Stage param)
   {
      adaptedClass.setStage(param);
   }

   public List<Condition.Evidence> getEvidence()
   {
      return adaptedClass.getEvidence();
   }

   public void setEvidence(List<Condition.Evidence> param)
   {
      adaptedClass.setEvidence(param);
   }

   public void addEvidence(Condition.Evidence param)
   {
      adaptedClass.getEvidence().add(param);
   }

   public List<Condition.Location> getLocation()
   {
      return adaptedClass.getLocation();
   }

   public void setLocation(List<Condition.Location> param)
   {
      adaptedClass.setLocation(param);
   }

   public void addLocation(Condition.Location param)
   {
      adaptedClass.getLocation().add(param);
   }

   public List<Condition.DueTo> getDueTo()
   {
      return adaptedClass.getDueTo();
   }

   public void setDueTo(List<Condition.DueTo> param)
   {
      adaptedClass.setDueTo(param);
   }

   public void addDueTo(Condition.DueTo param)
   {
      adaptedClass.getDueTo().add(param);
   }

   public List<Condition.OccurredFollowing> getOccurredFollowing()
   {
      return adaptedClass.getOccurredFollowing();
   }

   public void setOccurredFollowing(List<Condition.OccurredFollowing> param)
   {
      adaptedClass.setOccurredFollowing(param);
   }

   public void addOccurredFollowing(Condition.OccurredFollowing param)
   {
      adaptedClass.getOccurredFollowing().add(param);
   }

   public StringDt getNotesElement()
   {
      return adaptedClass.getNotesElement();
   }

   public String getNotes()
   {
      return adaptedClass.getNotes();
   }

   public void setNotes(String param)
   {
      adaptedClass.setNotes(new ca.uhn.fhir.model.primitive.StringDt(param));
   }

   public void setNotes(StringDt param)
   {
      adaptedClass.setNotes(param);
   }

   public Condition getAdaptee()
   {
      return adaptedClass;
   }

   public void setAdaptee(Condition param)
   {
      this.adaptedClass = param;
   }
}