package org.socraticgrid.fhir.generated;

import ca.uhn.fhir.model.primitive.IdDt;
import ca.uhn.fhir.model.primitive.CodeDt;
import ca.uhn.fhir.model.dstu2.composite.NarrativeDt;
import ca.uhn.fhir.model.dstu2.composite.ContainedDt;
import ca.uhn.fhir.model.dstu2.composite.IdentifierDt;
import java.util.List;
import ca.uhn.fhir.model.dstu2.resource.Patient;
import ca.uhn.fhir.model.dstu2.valueset.ProcedureStatusEnum;
import ca.uhn.fhir.model.primitive.BoundCodeDt;
import ca.uhn.fhir.model.dstu2.composite.CodingDt;
import java.util.ArrayList;
import java.util.Iterator;
import ca.uhn.fhir.model.dstu2.composite.CodeableConceptDt;
import ca.uhn.fhir.model.primitive.DateTimeDt;
import java.util.Date;
import ca.uhn.fhir.model.dstu2.composite.TimingDt;
import ca.uhn.fhir.model.dstu2.resource.Encounter;
import ca.uhn.fhir.model.dstu2.resource.Location;
import ca.uhn.fhir.model.primitive.StringDt;
import ca.uhn.fhir.model.dstu2.resource.Procedure;

public interface IProcedure
{

   public IdDt getId();

   public void setId(IdDt param);

   public CodeDt getLanguage();

   public void setLanguage(CodeDt param);

   public NarrativeDt getText();

   public void setText(NarrativeDt param);

   public ContainedDt getContained();

   public void setContained(ContainedDt param);

   public List<IdentifierDt> getIdentifier();

   public void setIdentifier(List<IdentifierDt> param);

   public void addIdentifier(IdentifierDt param);

   public Patient getPatientResource();

   public void setPatientResource(Patient param);

   public BoundCodeDt<ProcedureStatusEnum> getStatusElement();

   public String getStatus();

   public void setStatus(String param);

   public void setStatus(BoundCodeDt<ProcedureStatusEnum> param);

   public CodeableConceptDt getCategory();

   public void setCategory(CodeableConceptDt param);

   public List<String> getCategoryAsStringList();

   public CodeableConceptDt getType();

   public void setType(CodeableConceptDt param);

   public List<String> getTypeAsStringList();

   public List<Procedure.BodySite> getBodySite();

   public void setBodySite(List<Procedure.BodySite> param);

   public void addBodySite(Procedure.BodySite param);

   public List<CodeableConceptDt> getIndication();

   public void setIndication(List<CodeableConceptDt> param);

   public void addIndication(CodeableConceptDt param);

   public List<Procedure.Performer> getPerformer();

   public void setPerformer(List<Procedure.Performer> param);

   public void addPerformer(Procedure.Performer param);

   public DateTimeDt getPerformedDateTimeElement();

   public Date getPerformedDateTime();

   public void setPerformedDateTime(DateTimeDt param);

   public void setPerformedDateTime(Date param);

   public TimingDt getPerformedPeriod();

   public void setPerformedPeriod(TimingDt param);

   public Encounter getEncounterResource();

   public void setEncounterResource(Encounter param);

   public Location getLocationResource();

   public void setLocationResource(Location param);

   public CodeableConceptDt getOutcome();

   public void setOutcome(CodeableConceptDt param);

   public List<String> getOutcomeAsStringList();

   public List<CodeableConceptDt> getComplication();

   public void setComplication(List<CodeableConceptDt> param);

   public void addComplication(CodeableConceptDt param);

   public List<CodeableConceptDt> getFollowUp();

   public void setFollowUp(List<CodeableConceptDt> param);

   public void addFollowUp(CodeableConceptDt param);

   public List<Procedure.RelatedItem> getRelatedItem();

   public void setRelatedItem(List<Procedure.RelatedItem> param);

   public void addRelatedItem(Procedure.RelatedItem param);

   public StringDt getNotesElement();

   public String getNotes();

   public void setNotes(String param);

   public void setNotes(StringDt param);

   public List<Procedure.Device> getDevice();

   public void setDevice(List<Procedure.Device> param);

   public void addDevice(Procedure.Device param);

   public Procedure getAdaptee();

   public void setAdaptee(Procedure param);
}