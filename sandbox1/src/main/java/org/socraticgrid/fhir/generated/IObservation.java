package org.socraticgrid.fhir.generated;

import ca.uhn.fhir.model.primitive.IdDt;
import ca.uhn.fhir.model.primitive.CodeDt;
import ca.uhn.fhir.model.dstu2.composite.NarrativeDt;
import ca.uhn.fhir.model.dstu2.composite.ContainedDt;
import ca.uhn.fhir.model.dstu2.composite.CodingDt;
import java.util.ArrayList;
import java.util.Iterator;
import ca.uhn.fhir.model.dstu2.composite.CodeableConceptDt;
import java.util.List;
import ca.uhn.fhir.model.dstu2.composite.QuantityDt;
import ca.uhn.fhir.model.primitive.StringDt;
import ca.uhn.fhir.model.dstu2.composite.RangeDt;
import ca.uhn.fhir.model.dstu2.composite.RatioDt;
import ca.uhn.fhir.model.dstu2.composite.SampledDataDt;
import ca.uhn.fhir.model.dstu2.composite.AttachmentDt;
import ca.uhn.fhir.model.primitive.TimeDt;
import ca.uhn.fhir.model.primitive.DateTimeDt;
import java.util.Date;
import ca.uhn.fhir.model.dstu2.composite.TimingDt;
import ca.uhn.fhir.model.dstu2.valueset.ObservationValueAbsentReasonEnum;
import ca.uhn.fhir.model.dstu2.composite.BoundCodeableConceptDt;
import ca.uhn.fhir.model.dstu2.valueset.ObservationInterpretationCodesEnum;
import ca.uhn.fhir.model.primitive.InstantDt;
import ca.uhn.fhir.model.dstu2.valueset.ObservationStatusEnum;
import ca.uhn.fhir.model.primitive.BoundCodeDt;
import ca.uhn.fhir.model.dstu2.valueset.ObservationReliabilityEnum;
import ca.uhn.fhir.model.dstu2.composite.ResourceReferenceDt;
import ca.uhn.fhir.model.dstu2.composite.IdentifierDt;
import ca.uhn.fhir.model.dstu2.resource.Patient;
import ca.uhn.fhir.model.dstu2.resource.Group;
import ca.uhn.fhir.model.dstu2.resource.Device;
import ca.uhn.fhir.model.dstu2.resource.Location;
import ca.uhn.fhir.model.dstu2.resource.Specimen;
import ca.uhn.fhir.model.dstu2.resource.DeviceMetric;
import ca.uhn.fhir.model.dstu2.resource.Encounter;
import ca.uhn.fhir.model.dstu2.resource.Observation;

public interface IObservation
{

   public IdDt getId();

   public void setId(IdDt param);

   public CodeDt getLanguage();

   public void setLanguage(CodeDt param);

   public NarrativeDt getText();

   public void setText(NarrativeDt param);

   public ContainedDt getContained();

   public void setContained(ContainedDt param);

   public CodeableConceptDt getCode();

   public void setCode(CodeableConceptDt param);

   public List<String> getCodeAsStringList();

   public QuantityDt getValueQuantity();

   public void setValueQuantity(QuantityDt param);

   public CodeableConceptDt getValueCodeableConcept();

   public void setValueCodeableConcept(CodeableConceptDt param);

   public StringDt getValueStringElement();

   public String getValueString();

   public void setValueString(StringDt param);

   public void setValueString(String param);

   public RangeDt getValueRange();

   public void setValueRange(RangeDt param);

   public RatioDt getValueRatio();

   public void setValueRatio(RatioDt param);

   public SampledDataDt getValueSampledData();

   public void setValueSampledData(SampledDataDt param);

   public AttachmentDt getValueAttachment();

   public void setValueAttachment(AttachmentDt param);

   public TimeDt getValueTimeElement();

   public String getValueTime();

   public void setValueTime(TimeDt param);

   public void setValueTime(String param);

   public DateTimeDt getValueDateTimeElement();

   public Date getValueDateTime();

   public void setValueDateTime(DateTimeDt param);

   public void setValueDateTime(Date param);

   public TimingDt getValuePeriod();

   public void setValuePeriod(TimingDt param);

   public BoundCodeableConceptDt<ObservationValueAbsentReasonEnum> getDataAbsentReason();

   public void setDataAbsentReason(
         BoundCodeableConceptDt<ObservationValueAbsentReasonEnum> param);

   public BoundCodeableConceptDt<ObservationInterpretationCodesEnum> getInterpretation();

   public void setInterpretation(
         BoundCodeableConceptDt<ObservationInterpretationCodesEnum> param);

   public StringDt getCommentsElement();

   public String getComments();

   public void setComments(String param);

   public void setComments(StringDt param);

   public DateTimeDt getAppliesDateTimeElement();

   public Date getAppliesDateTime();

   public void setAppliesDateTime(DateTimeDt param);

   public void setAppliesDateTime(Date param);

   public TimingDt getAppliesPeriod();

   public void setAppliesPeriod(TimingDt param);

   public InstantDt getIssuedElement();

   public Date getIssued();

   public void setIssued(Date param);

   public void setIssued(InstantDt param);

   public BoundCodeDt<ObservationStatusEnum> getStatusElement();

   public String getStatus();

   public void setStatus(String param);

   public void setStatus(BoundCodeDt<ObservationStatusEnum> param);

   public BoundCodeDt<ObservationReliabilityEnum> getReliabilityElement();

   public String getReliability();

   public void setReliability(String param);

   public void setReliability(BoundCodeDt<ObservationReliabilityEnum> param);

   public CodeableConceptDt getBodySiteCodeableConcept();

   public void setBodySiteCodeableConcept(CodeableConceptDt param);

   public ResourceReferenceDt getBodySiteReference();

   public void setBodySiteReference(ResourceReferenceDt param);

   public CodeableConceptDt getMethod();

   public void setMethod(CodeableConceptDt param);

   public List<String> getMethodAsStringList();

   public List<IdentifierDt> getIdentifier();

   public void setIdentifier(List<IdentifierDt> param);

   public void addIdentifier(IdentifierDt param);

   public Patient getSubjectPatientResource();

   public void setSubjectResource(Patient param);

   public Group getSubjectGroupResource();

   public void setSubjectResource(Group param);

   public Device getSubjectDeviceResource();

   public void setSubjectResource(Device param);

   public Location getSubjectLocationResource();

   public void setSubjectResource(Location param);

   public Specimen getSpecimenResource();

   public void setSpecimenResource(Specimen param);

   public Device getDeviceDeviceResource();

   public void setDeviceResource(Device param);

   public DeviceMetric getDeviceDeviceMetricResource();

   public void setDeviceResource(DeviceMetric param);

   public Encounter getEncounterResource();

   public void setEncounterResource(Encounter param);

   public List<Observation.ReferenceRange> getReferenceRange();

   public void setReferenceRange(List<Observation.ReferenceRange> param);

   public void addReferenceRange(Observation.ReferenceRange param);

   public List<Observation.Related> getRelated();

   public void setRelated(List<Observation.Related> param);

   public void addRelated(Observation.Related param);

   public Observation getAdaptee();

   public void setAdaptee(Observation param);
}